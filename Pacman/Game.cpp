#include "Game.h"
#include "Ghost.h" //Testing
#include "Player.h"
#include "Animatable.h"
#include "Animation.h"
#include "TileMap.h"

#define SHEETP "../Resources/Graphics/Sheet2.png"
#define JINGLEP "../Resources/SFX/jingle.wav"

#define GameFieldWidth 10
#define GameFieldHeight 10
#define TileWidth 64
#define TileHeight 64

Game::Game()
{
	//Load Graphic and Audio
	mSpriteSheet.loadFromFile(SHEETP);
	mJingle.openFromFile(JINGLEP);

	//Create


	//Tile fillTile = Tile(2, 10);
	mGameField=&TileMap(mSpriteSheet, GameFieldWidth, GameFieldHeight, TileWidth, TileHeight);

	Tile tileBack = Tile(2, 10);
	uint tileBackID=mGameField->registerTile(tileBack);
	Tile tileSome = Tile(2, 9);
	uint tileSomeID = mGameField->registerTile(tileSome);

	//The first registered Tile is the tile, that is beeing used for the background

	mGameField->setTileAt(2, 2, tileSomeID);

	mRenderWindow.create(sf::VideoMode(TileWidth*GameFieldWidth, TileHeight*GameFieldHeight), "Pacman Game Mode");

	//Run
	start();
}

void Game::end() 
{

}

void Game::gameOver()
{

}

void Game::start()
{
	//Audio
	mJingle.play();

	//initialise Pacman
	Player pacman(20, 20, 200);
	pacman.setSpeed(500.0);
	pacman.setTexture(mSpriteSheet);

	//Initialise Ghosts
	Ghost blinky(300, 100, 100, sf::Color(255, 0, 0, 255));
	blinky.setTexture(mSpriteSheet);
	Ghost inky(300, 200, 100, sf::Color(0, 255, 0, 255));
	inky.setTexture(mSpriteSheet);
	Ghost pinky(300, 300, 100, sf::Color(0, 0, 255, 255));
	pinky.setTexture(mSpriteSheet);
	Ghost clyde(300, 400, 100, sf::Color(0, 255, 255, 255));
	clyde.setTexture(mSpriteSheet);

	//Main game loop
	while (mRenderWindow.isOpen())
	{
		//Poll events
		sf::Event event;
		while (mRenderWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) mRenderWindow.close();
		}

		//Update Textures and position
		pacman.update();
		pinky.update();
		inky.update();
		blinky.update();
		clyde.update();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) pacman.setMovement(Livable::UP);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) pacman.setMovement(Livable::DOWN);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) pacman.setMovement(Livable::LEFT);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) pacman.setMovement(Livable::RIGHT);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) pacman.setMovement(Livable::STOP);
		
		//Check Ghost collision
		if (pacman.isColliding(pinky) || pacman.isColliding(inky) || pacman.isColliding(blinky) || pacman.isColliding(clyde))
		{
			pacman.setMovement(Livable::STOP);
		}

		//Screen refresh
		mRenderWindow.clear();
		mGameField->draw(mRenderWindow);
		mRenderWindow.draw(pacman);
		mRenderWindow.draw(pinky);
		mRenderWindow.draw(inky);
		mRenderWindow.draw(blinky);
		mRenderWindow.draw(clyde);
		mRenderWindow.display();


	}
}