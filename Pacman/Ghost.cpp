#include "Ghost.h"
#include <iostream>


Ghost::Ghost(uint posX, uint posY, uint speed) :
	mAnimLookLeft(new Animation(64, 64, 1, 1, 2, 200)),
	mAnimLookRight(new Animation(64, 64, 1, 2, 2, 200)),
	mAnimLookDown(new Animation(64, 64, 1, 3, 2, 200)),
	mAnimLookUp(new Animation(64, 64, 1, 4, 2, 200)),
	mAnimTerrified(new Animation(64, 64, 1, 5, 2, 200)),
	mColor(255, 255, 255, 255),
	mIsDeadly(true)
{
	this->setAnimation(mAnimLookLeft);
	this->runAnimation();
	this->setPosition(posX, posY);
	this->setSpeed(speed);
	this->setMovement(Livable::RIGHT);
}

Ghost::Ghost(uint posX, uint posY, uint speed, sf::Color color) :
	mAnimLookLeft(new Animation(64, 64, 1, 1, 2, 200)),
	mAnimLookRight(new Animation(64, 64, 1, 2, 2, 200)),
	mAnimLookDown(new Animation(64, 64, 1, 3, 2, 200)),
	mAnimLookUp(new Animation(64, 64, 1, 4, 2, 200)),
	mAnimTerrified(new Animation(64, 64, 1, 5, 2, 200)),
	mColor(color),
	mIsDeadly(true)
{
	this->setColor(mColor);
	this->setAnimation(mAnimLookLeft);
	this->runAnimation();
	this->setPosition(posX, posY);
	this->setSpeed(speed);
	this->setMovement(Livable::RIGHT);
}

void Ghost::die()
{
	//Return to start position
}

void Ghost::setMovement(Livable::moveStates direction)
{
	this->setPositionMovement(direction);

	
	//Set animation accordingly
	if (mIsDeadly == true)
	{
		this->setColor(mColor);

		switch (direction)
		{
		case Livable::LEFT:
			setAnimation(mAnimLookLeft);
			break;
		case Livable::RIGHT:
			setAnimation(mAnimLookRight);
			break;
		case Livable::UP:
			setAnimation(mAnimLookUp);
			break;
		case Livable::DOWN:
			setAnimation(mAnimLookDown);
			break;
		case Livable::STOP:
			break;
		default:
			break;
		}
	}
}

void Ghost::setDeadly(bool isDeadly)
{
	mIsDeadly = isDeadly;
	if (mIsDeadly == false)
	{
		this->setColor(sf::Color::White);
		setAnimation(mAnimTerrified);
	}
	else {
		this->setMovement(this->getMoveState());
	}
}