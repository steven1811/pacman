#include "Animatable.h"
#include "Ghost.h"

//Possible Exception if no Animation is set
Animatable::Animatable()
{
	this->setTextureRect(sf::IntRect(1, 1, 1, 1));
}

void Animatable::runAnimation()
{
	mIsAnimationRunning = true;
}

void Animatable::setAnimation(Animation* animationP)
{
	mCurrentAnimation = animationP;
	animationP->getStartLocX();
	this->runAnimation();
}

void Animatable::stopAnimation()
{
	mIsAnimationRunning = false;
}

void Animatable::updateAnimation()
{
	if (mIsAnimationRunning == true)
	{
		if (mClock.getElapsedTime().asMilliseconds() > mCurrentAnimation->getDuration_ms()/ mCurrentAnimation->getKeyframeCount())
		{
			mClock.restart();
			if (mCurrentKeyframe < mCurrentAnimation->getKeyframeCount())
			{
				this->setTextureRect(sf::IntRect(
					((mCurrentAnimation->getStartLocX() - 1)*mCurrentAnimation->getTileWidth()) + ((mCurrentAnimation->getTileWidth())*mCurrentKeyframe),
					(mCurrentAnimation->getStartLocY() - 1)*mCurrentAnimation->getTileHeight(),
					mCurrentAnimation->getTileWidth(),
					mCurrentAnimation->getTileHeight()));
				mCurrentKeyframe++;
			}
			else
			{
				mCurrentKeyframe = 0;
			}
		}
	}

}