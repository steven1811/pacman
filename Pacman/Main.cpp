#include <SFML/Graphics.hpp>
#include "Game.h"
#include "Editor.h"



int main()
{
	//Mode chooser
	sf::RenderWindow mRenderWindow;
	
	//Font loader
	sf::Font standardFont;
	standardFont.loadFromFile("../Resources/Fonts/arial.ttf");
	
	//Mode Text
	sf::Text modeText("Please select a Mode.\nPress G for the game or E for the Editor", standardFont,32);
	modeText.setColor(sf::Color::Red);
	
	//Made by Text
	sf::Text createdText("Created by Steven Bradley(s0553035), Julian Ortel(s0553137), Oliver Wang(s000000)", standardFont, 16);
	createdText.setColor(sf::Color::Blue);
	createdText.setPosition(0, 500);

	//Background
	sf::Texture texture;
	texture.loadFromFile("../Resources/Graphics/Splashscreen.jpg");
	sf::Sprite background(texture);
	background.setScale(0.4f, 0.4f);
	
	//Renderer
	mRenderWindow.create(sf::VideoMode(800, 600), "Pacman Mode chooser");

	while (mRenderWindow.isOpen()) 

	{
		sf::Event event;
		//X Window Titlebar button clicked
		while (mRenderWindow.pollEvent(event)) 
		{
			if (event.type == sf::Event::Closed) mRenderWindow.close();
		}

		//Start game
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			mRenderWindow.close();
			Game();
		}

		//Start Editor
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			mRenderWindow.close();
			Editor();
		}

		//Clear buffer
		mRenderWindow.clear();

		//Draw calls could be implemented in a stack or list
		mRenderWindow.draw(background);
		mRenderWindow.draw(modeText);
		mRenderWindow.draw(createdText);
		

		//Refresh screen
		mRenderWindow.display();
	}
}