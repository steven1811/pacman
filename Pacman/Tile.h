#ifndef TILE_H
#define TILE_H
#include <SFML\Graphics.hpp>
#include "typedefs.h"

class Tile : public sf::Sprite
{
public:
	Tile(uint startLocX, uint startLocY) : mStartLocX(startLocX), mStartLocY(startLocY) {}
	bool isCollidable() { return mIsCollidable; }

	//Getter
	uint getStartLocX() { return mStartLocX; }
	uint getStartLocY() { return mStartLocY; }

private:
	uint mStartLocX;
	uint mStartLocY;

	//Flags
	bool mIsCollidable = false;
	bool mIsDeadly = false;

};
#endif