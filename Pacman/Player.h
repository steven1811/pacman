#ifndef PLAYER_H
#define PLAYER_H
#include "typedefs.h"
#include "Livable.h"
#include <SFML\Graphics.hpp>
#include <iostream>

class Player : public Livable
{
public:
	Player(uint posX, uint posY, uint speed);
	void setMovement(Livable::moveStates direction);
	void die();

private:
	Animation* mAnimMoving;
	Animation* mAnimDie;
};
#endif