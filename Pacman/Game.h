#ifndef GAME_H
#define GAME_H

#include "typedefs.h"
#include "TileMap.h"
#include <SFML\Audio.hpp>

class Game 
{
public:
	Game();
	void end();
	void gameOver();
	void start();

private:
	byte mDifficulty=0;
	byte mLives = 3;
	byte mScore = 0;
	unsigned int mTimer = 180;
	TileMap* mGameField;

	sf::RenderWindow mRenderWindow;
	sf::Texture mSpriteSheet;
	sf::Music mJingle;
};
#endif