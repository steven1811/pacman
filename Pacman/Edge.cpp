#include "Edge.h"


//---------------------------------------------------------------------------------------------------------------------

Edge::Edge(Node& rSrc, Node& rDst) : m_rSrc(rSrc), m_rDst(rDst)
{
	m_rSrc.getOutEdges().push_front(this);// f�gt die Edge (this) in m_outgoingEdges des Source-Node ein.
	m_rDst.getInEdges().push_front(this);// f�gt die Edge (this) in m_incomingEdges des Destination-Node ein.

    // Hinweis: die Funktionen Node::getOutEdges() und Node::getInEdges() verwenden!

    // TEST:
    // Erstellen Sie in main.cpp eine Edge und pr�fen Sie, ob die Edge
    // korrekt in m_outgoingEdges bzw. m_incomingEdges eingef�gt wurde!
}


//---------------------------------------------------------------------------------------------------------------------

Edge::Edge(const Edge& rOther) : m_rSrc(rOther.m_rSrc), m_rDst(rOther.m_rDst)
{
    // macht das Selbe wie 'Edge(Node& rSrc, Node& rDst)'
	m_rSrc.getOutEdges().push_front(this);
	m_rDst.getInEdges().push_front(this);
}


//---------------------------------------------------------------------------------------------------------------------

Edge::~Edge()
{
	m_rSrc.getOutEdges().remove(this);// - entfernt die Edge (this) aus m_outgoingEdges im Source-Node
	m_rSrc.getInEdges().remove(this);// - entfernt die Edge (this) aus m_incomingEdges im Destination-Node

    // TEST:
    // Erstellen Sie in main.cpp drei Edges, die jeweils den selben Source- und Destination-Node haben.
    // L�schen Sie eine Edge wieder!
    // Pr�fen Sie, ob die gel�schte Edge aus m_outgoingEdges bzw. m_incomingEdges entfernt wurde!

    // Hinweis: arbeiten Sie mit 'new' und 'delete'!
}


//---------------------------------------------------------------------------------------------------------------------

bool Edge::isConnectedTo(const Node& rNode)
{
	return (&rNode == &m_rSrc) || (&m_rDst == &rNode);

    // - gibt true zur�ck, wenn rNode entweder m_rSrc und m_rDst ist.
    // Hinweis: Adressen vergleichen, um zu gucken, ob es wirklich das selbe Objekt ist!
    return false;
}


//---------------------------------------------------------------------------------------------------------------------

std::string Edge::toString() const
{
    std::string result;
    result = m_rSrc.getID() + " -> " + m_rDst.getID();
    return result;
}


//---------------------------------------------------------------------------------------------------------------------
