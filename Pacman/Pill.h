#ifndef PILL_H
#define PILL_H
#include "typedefs.h"
#include "Effect.h"
#include <SFML\Graphics.hpp>

class Pill
{
public:
	Pill(uint posX, uint posY, Effect *effect);

	//Getter
	Effect* getEffect() { return &mEffect; };
	uint getPosX() { return mPosX; };
	uint getPosY() { return mPosY; };
	sf::Sprite* getSprite() { return &mSprite; };
	sf::Color* getColor() { return &mColor; };

private:
	Effect mEffect;
	uint mPosX;
	uint mPosY;
	sf::Sprite mSprite;
	sf::Color mColor;
};
#endif