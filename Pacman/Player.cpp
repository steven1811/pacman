#include "Player.h"
#include <iostream>

Player::Player(uint posX, uint posY, uint speed) :
	mAnimMoving(new Animation(64, 64, 1, 7, 2, 200)),
	mAnimDie(new Animation(64, 64, 1, 8, 10, 800))
{
	this->setAnimation(mAnimMoving);
	this->runAnimation();
	this->setPosition(posX, posY);
	this->setSpeed(speed);
	setMovement(RIGHT);
	setOrigin(32, 32);
}

void Player::setMovement(Livable::moveStates direction)
{
	this->setPositionMovement(direction);

	switch (direction)
	{
	case Livable::LEFT:
		setRotation(180);
		break;
	case Livable::RIGHT:
		setRotation(0);
		break;
	case Livable::UP:
		setRotation(270);
		break;
	case Livable::DOWN:
		setRotation(90);
		break;
	case Livable::STOP:
		break;
	default:
		break;
	}
}

void Player::die(){
		this->setMovement(STOP);
		this->setAnimation(mAnimDie);
		this->runAnimation();
}