#ifndef ANIMATION_H
#define ANIMATION_H
#include "typedefs.h"

class Animation
{
public:
	Animation(uint tileWidth, uint tileHeight, uint startLocX, uint startLocY, uint keyFrameCount, ulong duration_ms) :
		mTileWidth(tileWidth), mTileHeight(tileHeight),
		mCountKeyframes(keyFrameCount), mStartLocX(startLocX),
		mStartLocY(startLocY), mDuration_ms(duration_ms) {}

	//Getter
	uint getKeyframeCount() { return mCountKeyframes; }
	ulong getDuration_ms() { return mDuration_ms; }
	uint getTileHeight() { return mTileHeight; }
	uint getTileWidth() { return mTileWidth; }
	uint getStartLocX() { return mStartLocX; }
	uint getStartLocY() { return mStartLocY; }

	//Setter 
	void setKeyframeCount(uint keyframeCount) { mCountKeyframes=keyframeCount; }
	void setDuration_ms(ulong duration_ms) { mDuration_ms=duration_ms;}
	void setTileWidth(uint tileWidth) { mTileWidth=tileWidth;}
	void setTileHeight(uint tileHeight) { mTileHeight=tileHeight;}
	void setStartLocX(uint startLocX) {mStartLocX=startLocX; }
	void setStartLocY(uint startLocY) { mStartLocY=startLocY; }

private:
	uint mCountKeyframes;
	ulong mDuration_ms;
	uint mTileWidth;
	uint mTileHeight;
	uint mStartLocX;
	uint mStartLocY;
};
#endif