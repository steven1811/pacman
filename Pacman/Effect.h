#ifndef EFFECT_H
#define EFFECT_H
#include "typedefs.h"
#include "Livable.h"

class Effect 
{
public:
	Effect();
	void trigger(Livable *target);

	//Getter
	uint getDuration() { return mDuration; };
private:
	uint mDuration;
};
#endif