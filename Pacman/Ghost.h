#ifndef GHOST_H
#define GHOST_H
#include "typedefs.h"
#include <SFML\Graphics.hpp>
#include "Livable.h"
#include <iostream>

class Ghost : public Livable
{
public:
	Ghost(uint posX, uint posY, uint speed);
	Ghost(uint posX, uint posY, uint speed, sf::Color color);

	void die();
	void setMovement(Livable::moveStates direction);

	//Setter
	void setDeadly(bool isDeadly);

	//Getter
	sf::Color* getColor() { return &mColor; };
	bool isDeadly() { return mIsDeadly; };

private:
	Animation* mAnimLookLeft;
	Animation* mAnimLookRight;
	Animation* mAnimLookDown;
	Animation* mAnimLookUp;
	Animation* mAnimTerrified;

	sf::Color mColor;
	bool mIsDeadly;
};
#endif