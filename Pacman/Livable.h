#ifndef LIVABLE_H
#define LIVABLE_H
#include "typedefs.h"
#include <SFML\Graphics.hpp>
#include "Animatable.h"

//pure abstract class. Never gets instantiated. Cannot exist on its own.
class Livable : public Animatable
{
public:

	enum moveStates {
		STOP, LEFT, RIGHT, UP, DOWN
	};

	Livable() : mMoveState(STOP) {};
	virtual void die() = 0; //Needs to be overwritten. Behaviour depends on its entity.

	//Setter
	void setSpeed(float speed) { mSpeed = speed; }

	//Has to return Object
	bool isColliding(Livable& livable);

	//Getter
	float getSpeed() { return mSpeed; }
	moveStates getMoveState() { return mMoveState; }
	void update();

protected:
	void setPositionMovement(moveStates moveState) { mMoveState = moveState; }

private:
	sf::Clock mClock;
	moveStates mMoveState;
	float mSpeed;
};
#endif