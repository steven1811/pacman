#ifndef TILEMAP_H
#define TILEMAP_H
#include "typedefs.h"
#include <SFML\Graphics.hpp>
#include "Tile.h"

class TileMap
{
public:
	TileMap(sf::Texture & tileMapTexture, uint sizeX, uint sizeY, uint tileSizeX, uint tileSizeY);
	uint registerTile(Tile tile);
	void setTileAt(uint posX, uint posY, uint TileID);
	//Getter
	uint getSizeX() { return mSizeX; }
	uint getSizeY() { return mSizeY; }
	uint getTileSizeX() { return mTileSizeX; }
	uint getTileSizeY() { return mTileSizeY; }

	void draw(sf::RenderTarget& target);
private:
	uint mSizeX;
	uint mSizeY;
	uint mTileSizeX;
	uint mTileSizeY;

	sf::Texture* mTileMapTexture;
	std::vector<Tile> mTileSet;

	//Two dimensional array
	uint** mTileIDMap;
};
#endif