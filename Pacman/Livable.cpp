#include "Livable.h"

void Livable::update()
{
	float delta = mClock.restart().asSeconds();
	//Update Position

	switch (mMoveState)
	{
		case Livable::LEFT:
			this->move((-1)*mSpeed * delta, 0.0f);
			break;
		case Livable::RIGHT:
			this->move(mSpeed * delta, 0.0f);
			break;
		case Livable::UP:
			this->move(0.0f, -1 * mSpeed * delta);
			break;
		case Livable::DOWN:
			this->move(0.0f, mSpeed * delta);
		break;
		case Livable::STOP:
			break;
		default:
			break;
	}


	//Update Animation
	this->updateAnimation();
}

//Is colliding with a livable (Ghost, Pacman)? Bounding box collision check
bool Livable::isColliding(Livable& livable)
{
	if (
		this->getPosition().y + this->getGlobalBounds().height-10 < livable.getPosition().y ||
		this->getPosition().y > livable.getPosition().y + livable.getGlobalBounds().height - 10 ||
		this->getPosition().x + this->getGlobalBounds().width - 10 < livable.getPosition().x ||
		this->getPosition().x > livable.getPosition().x + livable.getGlobalBounds().width - 10) return false;
	return true;
}