#include "TileMap.h"

TileMap::TileMap(sf::Texture &tileMapTexture, uint sizeX, uint sizeY, uint tileSizeX, uint tileSizeY) : mSizeX(sizeX), mSizeY(sizeY), mTileSizeX(tileSizeX), mTileSizeY(tileSizeY), mTileMapTexture(&tileMapTexture)
{
	//Allocate memory to two dimensional Matrix
	mTileIDMap = (uint**)calloc(sizeX, sizeof(*mTileIDMap));
	for (uint i = 0; i < sizeX; i++)
	{
		mTileIDMap[i] = (uint*)calloc(sizeY, sizeof(*mTileIDMap));
	}

	//Fill our Map with the first ID
	for (uint y = 0; y < sizeY; y++)
	{
		for (uint x = 0; x < sizeX; x++)
		{
			mTileIDMap[x][y] = 0;
		}
	}
}

uint TileMap::registerTile(Tile tile)
{
	tile.setTexture(*mTileMapTexture);
	tile.setTextureRect(sf::IntRect((tile.getStartLocX() - 1)*mTileSizeX, (tile.getStartLocY() - 1)*mTileSizeY, mTileSizeX, mTileSizeY));
	mTileSet.push_back(tile);
	return mTileSet.size() - 1;
}

void TileMap::draw(sf::RenderTarget& target)
{
	for (uint y = 0; y < mSizeY; y++)
	{
		for (uint x = 0; x < mSizeX; x++)
		{
			//target.draw(mTileSet[mTileIDMap[x][y]]);
			mTileSet[mTileIDMap[x][y]].setPosition((x)*mTileSizeX, (y)*mTileSizeY);
			target.draw(mTileSet[mTileIDMap[x][y]]);
		}
	}

	//Draw each Tile to target
}

//Possible exception or true or false, if the tile is out of bounds
void TileMap::setTileAt(uint posX, uint posY, uint TileID)
{
	mTileIDMap[posX - 1][posY - 1] = TileID;

}