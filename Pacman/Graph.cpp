﻿#include "Graph.h"
#include <list>
#include <fstream>
#include <algorithm>
#include <limits>


//---------------------------------------------------------------------------------------------------------------------

Graph::~Graph()
{
	
	m_edges.clear(); // - soll alle Edges im Graph löschen (delete)
	m_nodes.clear(); // - soll alle Nodes im Graph löschen (delete)
}


//---------------------------------------------------------------------------------------------------------------------

Node& Graph::createNode(const std::string& id)
{
    // implementieren Sie vorher die Funktion 'findNode' (siehe weiter unten) !
    
    // bitte diese Zeile entfernen, wenn Sie die Funktion implementieren:
    //return Node("");

    // Überprüfen Sie, ob schon ein Node mit der gegeben id im Graph vorhanden ist!
	Node *ref = findNode(id);
	if (ref == NULL) {
		Node *node = new Node();
		m_nodes.push_front(node);
		ref = node;
	}
	return *ref;
	// Falls ja:
    //  - Referenz auf diesen Node zurück geben
    // Falls nein:
    //  - neuen Node erstellen (mit new)
    //  - den neuen Node in m_nodes einfügen
    //  - Referenz auf den neuen Node zurück geben

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes in main.cpp erstellen
    // Testen Sie mit der Funktion 'findNode', ob die hinzugefügten Nodes im Graph vorhanden sind.
}


//---------------------------------------------------------------------------------------------------------------------

Edge& Graph::createEdge(Node& rSrcNode, Node& rDstNode)
{
    // bitte diese Zeile entfernen, wenn Sie die Funktion implementieren:
    //return Edge(rSrcNode, rDstNode);

	bool foundSrc=false;
	bool foundDst=false;
	std::list<Node *>::iterator it=m_nodes.begin();
	for (; it != m_nodes.end(); ++it) {
		if ((*it) == &rSrcNode) {
			foundSrc = true;
		}

		if ((*it) == &rDstNode) {
			foundDst = true;
		}
	}

	if (foundSrc == false) {
		m_nodes.push_front(&rSrcNode);
	}
	if (foundDst == false) {
		m_nodes.push_front(&rDstNode);
	}

	Edge *edge = new Edge(rSrcNode, rDstNode);
	m_edges.push_front(edge);
	return *edge;
	
	// - rSrcNode zum Graph hinzufügen, falls er noch nicht in m_nodes vorhanden ist.
    // - rDstNode zum Graph hinzufügen, falls er noch nicht in m_nodes vorhanden ist.
    // - neue Edge erstellen (mit new)
    // - die neue Edge in m_edges einfügen
    // - Referenz auf die neue Edge zurück geben

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes und Edges in main.cpp erstellen
    // Testen Sie mit der Funktion 'findEdges', ob die hinzugefügten Edges im Graph vorhanden sind.
}


//---------------------------------------------------------------------------------------------------------------------

void Graph::remove(Node& rNode)
{
    // implementieren Sie vorher die Funktion 'remove(Edge& rEdge)' !

    // - alle Edges, die mit rNode verbunden sind, müssen entfernt werden!
    // - der Pointer auf rNode soll aus m_nodes entfernt werden!
    // - der Pointer auf rNode muss mit 'delete' freigegeben werden!

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes und Edges in main.cpp erstellen
    // und anschließend einzelne Nodes wieder löschen.
    // Testen Sie mit der Funktion 'findNode', ob die gelöschten Nodes noch im Graph vorhanden sind.
}


//---------------------------------------------------------------------------------------------------------------------

void Graph::remove(Edge& rEdge)
{
    // - der Pointer auf rEdge muss aus m_edges entfernt werden!
    // - der Pointer auf rEdge muss mit 'delete' freigegeben werden!

	if (std::find(m_edges.begin(), m_edges.end(), &rEdge) != m_edges.end()) {
		m_edges.remove(&rEdge);
		delete &rEdge;
	}

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes und Edges in main.cpp erstellen
    // und anschließend einzelne Edges wieder löschen.
    // Testen Sie mit der Funktion 'findEdges', ob die gelöschten Edges noch im Graph vorhanden sind.
}


//---------------------------------------------------------------------------------------------------------------------


Node* Graph::findNode(const std::string& id)
{
	std::list<Node *>::iterator nodesIterator = m_nodes.begin();

	// - soll einen Node mit der gegebenen id in m_nodes suchen

	for (; nodesIterator != m_nodes.end(); ++nodesIterator) {
		if ((*nodesIterator)->getID() == id) {
			return *nodesIterator;     // - gibt den Pointer auf den Node zurück, wenn er gefunden wurde.
		}
	}

	return NULL;// - gibt NULL zurück, falls kein Node mit der id gefunden wurde.

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes und Edges in main.cpp erstellen
    // und anschließend ein paar Nodes im Graph suchen.
    // Prüfen Sie, ob der Node gefunden wurden und geben Sie die ID auf der Kommandozeile aus!
}


//---------------------------------------------------------------------------------------------------------------------

std::vector<Edge*> Graph::findEdges(const Node& rSrc, const Node& rDst)
{
    std::vector<Edge*> ret;

    // - findet alle edges, mit rSrc als Source-Node und rDst als Destination-Node.

    // - füge die Zeiger der Edges in den vector 'ret' ein.

    return ret;

    // TEST:
    // Testen Sie die Funktion, indem Sie indem Sie einen Graph mit ein paar Nodes und Edges in main.cpp erstellen
    // und anschließend ein paar Edges im Graph suchen.
    // Prüfen Sie, ob Edges gefunden wurden und geben Sie die gefunden Edges auf der Kommandozeile aus!
}


//---------------------------------------------------------------------------------------------------------------------


void Graph::saveAsDot(const std::string& rFilename)
{
/*
Kopieren Sie den Ordner ‚Peters\C23_Algorithmen_Programmierung\Tools\graphviz‘ im Dozentenserver auf ein lokales Laufwerk.

Graphiz ist ein Tool, welches Graphen aus einer textuellen Beschreibung erzeugen kann.
Betrachten Sie die Datei graph.dot.
Formal ist die Datei folgendermaßen aufgebaut :

digraph {
concentrate = true
<NODE_ID>;
<NODE_ID>;
<NODE_ID>;
<EDGE_1>;
<EDGE_2>;
<EDGE_N>;
}

Starten Sie die Datei make.bat, damit Graphiz ein Bild des Graphen erstellt.
Spielen Sie mit der Datei graph.dot herum und lassen Sie sich den Graph dazu generieren.


Implementieren Sie nun die Funktion 'Graph::saveAsDot', damit sie eine Dot-Datei im angegebenen Dateipfad erzeugt!

Hilfestellung:
- Dateien speichern mit C++: http://www.cplusplus.com/reference/fstream/fstream/open/
- Verwenden Sie die Funktionen Node::getID() und Edge::toString(), um die Einträge für die Nodes und Edges in der dot - Datei zu generieren.


TEST:
Testen Sie die Funktion, indem Sie indem Sie einen Graph in mit ein paar Nodes und Edges in main.cpp erstellen
und anschließend eine dot-Datei generieren. Erstellen Sie ein Bild des Graphen mit Graphviz.

*/


}


//---------------------------------------------------------------------------------------------------------------------

void Graph::findShortestPathDijkstra(std::deque<Edge*>& rPath, const Node& rSrcNode, const Node& rDstNode)
{
/*
Ein häufiges Anwendungsproblem für Graphen-Anwendungen besteht darin, 
den Pfad zwischen verschiedenen Nodes zu finden, die direkt oder indirekt über Edges miteinander verbunden sind.
Um den optimalsten Pfad(den mit den geringsten Kantengewichten) zu finden, gibt es den Dijkstra-Algorithmus!
Pseudocode (Quelle: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)
>>>
function Dijkstra(Graph, source):

      create vertex set Q

      for each vertex v in Graph:             // Initialization
          dist[v] ← INFINITY                  // Unknown distance from source to v
          prev[v] ← UNDEFINED                 // Previous node in optimal path from source
          add v to Q                          // All nodes initially in Q (unvisited nodes)

      dist[source] ← 0                        // Distance from source to source

      while Q is not empty:
          u ← vertex in Q with min dist[u]    // Source node will be selected first
          remove u from Q

          for each neighbor v of u:           // where v is still in Q.
              alt ← dist[u] + length(u, v)
              if alt < dist[v]:               // A shorter path to v has been found
                  dist[v] ← alt
                  prev[v] ← u

      return dist[], prev[]
<<<

Betrachten Sie den Pseudocode und setzen Sie ihn in C++ um.
Sortieren Sie am Ende das Ergebnis in die richtige Reihenfolge um 
und geben sie die kürzeste Route zwischen rSrcNode und rDstNode als Liste von Edges zurück.

TEST:
Testen Sie diese Funktion, indem Sie einen Graph in main.cpp erstellen
und sich die kürzesteste Route zwischen 2 Nodes zurückgeben lassen.
*/

}


//---------------------------------------------------------------------------------------------------------------------

