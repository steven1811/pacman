#ifndef ANIMATABLE_H
#define ANIMATABLE_H
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Animation.h"
#include <vector>
#include <iostream>

//TODO: Needs to be somewhat abstract, so that it can't exist on it's own.

class Animatable : public sf::Sprite
{
public:
	Animatable();
protected:
	void runAnimation();
	void setAnimation(Animation* animationP);
	void stopAnimation();
	void updateAnimation();

private:
	uint mCurrentKeyframe;
	bool mIsAnimationRunning;
	Animation* mCurrentAnimation;
	sf::Clock mClock;
};
#endif