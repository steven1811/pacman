uniform sampler2D texture;
uniform float pixel_threshold;

void main()
{
    float factor = 1.0 / (pixel_threshold + 0.001);
    vec2 pos = floor(gl_TexCoord[0].xy * factor + 0.5) / factor;
    gl_FragColor = texture2D(texture, pos) * gl_Color;
}

I'm half German (from mother and grandmother) half Brit (from father and grandfather), living and born in Germany.
A lot of your arguments and explanations come down to this: I just feel... Mostly... Maybe... Might... I feel that... It's like... I strongly believe...

I'm just not interested in emotional appeals and arguments that contain weasel words. Btw. Open up the comments press CTRL+F and type feel. Look how many results you will get. Your feelings are irrelevant on the discussion of Brexit.

Some points you made I want to address:

1. "Staying together is better.":
Agreed, that's why you need to leave the EU. The vote was about preserving your culture. It's not like you're leaving the planet earth. You will still have relationships with other countries. Countries want to trade with England and that goes both ways. On the other hand Multiculturalism has failed.

2. "I've received nasty comments/mails.":
Why are you focusing on those comments? Don't give a fuck if people write you nasty shit or threatening to harm you. It's the fucking Internet and you will receive negative comments. I'm certain that most people express their dis/agreement in their individual stylized way.

3. "We're on the path of the unknown":
It's better to take the path of the unknown than certain failure. History has shown us, that big governing bodies just don't work and will end in an catastrophe.

4. The assumption that right-wing is automatically bad:
This is a common false dichotomy you are paying credence to. I would consider me a left leaning center classic liberal myself and don't think that arguments of right-wingers should be automatically considered bad and dismissed. You actually look at those arguments and decide for yourself if you are convinced or not, with trying putting the least amount of bias that is possible. People can decide whether they consider me right or left. I don't play that stupid game anymore. My statement about my political position is based on the result of my political compass tests. Right-wingers will call me right-wing and left-wingers call me left, when they happen to agree with me on specific issues.

Yeah this post will most likely get the reaction of:" TOO LONG! DIDN'T READ!"  tl;dr


I think a youtube channel is good enough tho. Since my opinions are not that popular here in Germany, where I get called a racist Nazi for the slightest criticism and for presenting facts of Islam or talking about some uncomfortable truths and facts that are here in Germany.